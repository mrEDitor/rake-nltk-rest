#!/usr/bin/env python3
""" Copyright 2018, Eduard Minasyan <mrEDitor@mail.ru>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

""" Keywords extracting tool: nltk_rake wrapper.

Read text from standard input, process it to keywords
and print result to standard output line-by-line.
Use NLTK_DATA environment value to specify nltk data path.

Usage:
    keywords_extractor.py [--lang=LANG] [--count=COUNT]
where
    LANG specify text processing language;
    COUNT specify max count of keywords to print.

E.g.
    keywords_extractor.py --lang=english --count=7
"""


def main(text: str, lang: str = 'english',
         count: (int, None) = None, nltk_path: (str, None) = None):
    """ Extract keywords from given text.
    :param text: Text to process.
    :param lang: Text language to use.
    :param count: Count of keywords to return.
    :param nltk_path: Path to nltk data directory.
    :return: Tags
    """
    import nltk
    if nltk_path is not None:
        # noinspection PyProtectedMember
        nltk.downloader._downloader._download_dir = nltk_path
        nltk.data.path.insert(0, nltk_path)
    from rake_nltk import Rake
    rake = Rake(language=lang.lower())
    rake.extract_keywords_from_text(text)
    keywords = rake.get_ranked_phrases()
    if count is not None:
        keywords = keywords[:count]
    return keywords


if __name__ == '__main__':
    from argparse import ArgumentParser
    import sys

    parser = ArgumentParser(description='Extract keywords from text.')
    parser.add_argument('--lang', help='text language')
    parser.add_argument('--count', type=int, help='count of keywords in result')
    args = parser.parse_args()
    print(', '.join(main(sys.stdin.read(), lang=args.lang, count=args.count)))
