# rake-nltk RESTful API

### API methods

##### POST /extract_keywords
*Extract keywords from text.*

Parameters:
- **text**: Text to process.
- **lang**: Text language (optional).
- **count**: Max count of keywords to response (optional).

Response:
- JSON-object of following form:
    - `{"status": "success", "keywords": ["Keywords", "and", "keyphrases"]}`;
    - `{"status": "error", "message": "Human-readable error description"}`;

E.g. (hit Ctrl+D to send EOF when typing request body)

    $ curl -XPOST http://localhost/extract_keywords -d"`tr '\r\n' ' '`"

    {"text": "Compatibility of systems of linear constraints over the set of
    natural numbers. Criteria of compatibility of a system of linear
    Diophantine equations, strict inequations, and nonstrict inequations are
    considered. Upper bounds for components of a minimal set of solutions and
    algorithms of construction of minimal generating sets of solutions for all
    types of systems are given. These criteria and the corresponding algorithms
    for constructing a minimal supporting set of solutions can be used in
    solving all the considered types of systems and systems of mixed types.",
    "lang": "en", "count": 30}

    {"status": "success", "keywords": ["minimal generating sets", "linear
    diophantine equations", "minimal supporting set", "minimal set", "linear
    constraints", "upper bounds", "strict inequations", "nonstrict
    inequations", "natural numbers", "mixed types", "corresponding algorithms",
    "considered types", "set", "types", "considered", "algorithms", "used",
    "systems", "system", "solving", "solutions", "given", "criteria",
    "construction", "constructing", "components", "compatibility"]}

### Requirements

- Python 3 with `nltk` and `rake_nltk` libraries.
- CGI-wrapper for python scripts, such as `fcgiwrap`, **or** `flask` +
    `flask_restful` libraries for standalone server.

### Installation

##### With CGI
1. Get distributable files. Most recent ones you can get directly from project
    git repository at https://gitlab.com/mrEDitor/rake-nltk-rest/
2. Setup CGI to be able to run Python scripts as HTTP-service. E.g. `nginx` +
    `fastcgi` + `fcgiwrap` as described at
    https://www.nginx.com/resources/wiki/start/topics/examples/fcgiwrap/
3. Install `nltk` and `rake_nltk` Python packages.
4. Copy obtained distributable files to server.
5. Check that server has write access to `nltk_data` directory.
6. Make symbolic links to public API method scripts from your public CGI
    directory, e.g. `/var/www/localhost/cgi-bin`.
7. Optionally you can change configuration files in `config` directory.

##### As standalone server
1. Get distributable files. Most recent ones you can get directly from project
    git repository at https://gitlab.com/mrEDitor/rake-nltk-rest/
2. Install `nltk`, `rake_nltk`, `flask` and `flask_restful` Python packages.
3. Copy obtained distributable files to server.
4. Check that current user has write access to `nltk_data` directory.
5. Optionally you can change configuration files in `config` directory.
6. Run `run_standalone.py` script in Python 3. 

### License

API wrapper is licensed under Apache License 2.0. See LICENSE file for details.

See also https://github.com/csurfer/rake-nltk/blob/master/LICENSE for
rake-nltk license itself. 
