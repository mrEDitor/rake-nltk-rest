#!/usr/bin/env python3
""" Copyright 2018, Eduard Minasyan <mrEDitor@mail.ru>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

""" POST /extract_keywords
*Extract keywords from text.*

Parameters:
- **text**: Text to process.
- **lang**: Text language (optional).
- **count**: Max count of keywords to response (optional).

Response:
- JSON-array of keywords.

E.g. (hit Ctrl+D to send EOF when typing request body)

    $ curl -XPOST http://localhost/extract_keywords -d"`tr '\r\n' ' '`"

    {"text":"Compatibility of systems of linear constraints over the set of
    natural numbers. Criteria of compatibility of a system of linear
    Diophantine equations, strict inequations, and nonstrict inequations are
    considered. Upper bounds for components of a minimal set of solutions and
    algorithms of construction of minimal generating sets of solutions for all
    types of systems are given. These criteria and the corresponding algorithms
    for constructing a minimal supporting set of solutions can be used in
    solving all the considered types of systems and systems of mixed types.",
    "lang":"en"}

    {"status": "success", "keywords": ["minimal generating sets", "linear
    diophantine equations", "minimal supporting set", "minimal set", "linear
    constraints", "upper bounds", "strict inequations", "nonstrict
    inequations", "natural numbers", "mixed types", "corresponding algorithms",
    "considered types", "set", "types", "considered", "algorithms", "used",
    "systems", "system", "solving", "solutions", "given", "criteria",
    "construction", "constructing", "components", "compatibility"]}
"""

import json
import os
import sys
from collections.abc import Mapping
from configparser import ConfigParser

from lib import keywords_extractor

app_path = os.path.dirname(os.path.realpath(__file__))
app_action = os.path.basename(__file__).split('.', -1)[0]
config = ConfigParser()

# noinspection PyTypeChecker
config.read(os.path.join(app_path, 'config', app_action + '.cfg'))
# noinspection PyTypeChecker
config.read(os.path.join(app_path, 'config', app_action + '.local.cfg'))


def cgi_response(data: Mapping, status='200 OK'):
    """ Response with data as AJAX to cgi.
    :param data: Data dictionary to response.
    :param status: HTTP status.
    """
    print('Status: ' + status)
    print('Content-Type: application/json')
    print()
    print(json.dumps(data))


def error_response(message: str):
    """ Response with error status and message.
    :param message: Error message string.
    :return:
    """
    return {
        'status': 'error',
        'message': message,
    }


def success_response(data: Mapping):
    """ Response with success status and data.
    :param data: Data dictionary.
    :return:
    """
    result = {'status': 'success'}
    result.update(data)
    return result


def main(arguments: Mapping):
    """ Process extract_keywords request
    :param arguments: Action data.
    :return:
    """
    try:
        text = arguments['text']
    except KeyError:
        return error_response('Required request field missing: text')

    try:
        lang = config['language-mapping'][arguments['lang']]
    except KeyError:
        lang = config['language-mapping']['default']

    try:
        count = int(arguments['count'])
    except KeyError:
        count = None

    nltk_path = os.path.join(app_path, 'nltk_data')
    keywords = keywords_extractor.main(text, lang=lang, count=count, nltk_path=nltk_path)
    return success_response({'keywords': keywords})


if __name__ == '__main__':
    try:
        if os.environ['REQUEST_METHOD'] == 'POST':
            _data = json.loads(sys.stdin.read())
            cgi_response(main(_data))
        else:
            _message = 'Only POST method requests are supported.'
            cgi_response(error_response(_message), '400 Bad Request')
    except KeyError:
        print('[ERROR] Only CGI-context is supported.')
    except (TypeError, ValueError):
        _message = 'Request body must contain JSON object.'
        cgi_response(error_response(_message), '400 Bad Request')
