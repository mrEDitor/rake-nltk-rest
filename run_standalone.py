#!/usr/bin/env python3
""" Copyright 2018, Eduard Minasyan <mrEDitor@mail.ru>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import json
import os
from configparser import ConfigParser

from flask import Flask
from flask_restful import Resource, Api
# noinspection PyProtectedMember
from flask_restful import request

import extract_keywords

""" Standalone flask-based HTTP server. """

app = Flask(__name__)
# noinspection PyTypeChecker
api = Api(app)
app_path = os.path.dirname(os.path.realpath(__file__))
app_action = os.path.basename(__file__)
config = ConfigParser()
config.read(os.path.join(app_path, 'config', app_action + '.cfg'))
config.read(os.path.join(app_path, 'config', app_action + '.local.cfg'))


@api.resource('/extract_keywords')
class ExtractKeywords(Resource):
    """ /extract_keywords """

    # noinspection PyMethodMayBeStatic
    def post(self):
        """ POST /extract_keywords """
        return extract_keywords.main(json.loads(request.data))


if __name__ == '__main__':
    app.run(port=config.get('standalone-server', 'port', fallback=5000))
